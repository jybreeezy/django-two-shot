from django.shortcuts import render, redirect
from .models import Receipt
from django.contrib.auth.decorators import login_required
from .forms import ReceiptForm


# Create your views here.


@login_required
def receipt_list(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {
        'receipts': receipt_list
    }
    return render(request, 'receipts/receipt_list.html', context)

def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        receipt_list = form.save(commit=False)
        receipt_list.purchaser = request.user
        receipt_list.save()
        return redirect('home')
    else:
        form = ReceiptForm()
    context = {
        'form': form
    }
    return render(request, 'receipts/create_receipt.html', context)
